<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

// Системные пути
define('__CORE__',    realpath(__DIR__ . '/core'));
define('__APP__',     realpath(__DIR__ . '/app'));
define('__PUBLIC__',  realpath(__DIR__ . '/public'));

// Заголовок
header('Content-Type: text/html; charset=utf-8');

// Разбор uri в массив
$URI = strstr($_SERVER['REQUEST_URI'], '?', true);
$URI = explode('/', rtrim($URI ? $URI : $_SERVER['REQUEST_URI'], '/'));

// По умолчанию путь /catalog
if (count($URI) == 1) {
  header('Location: /catalog');
  die;
}

// Необходимые функции и подключение к БД
require_once realpath(__CORE__ . '/db.inc.php');
require_once realpath(__CORE__ . '/funcs.inc.php');

// Контроллер
require_once realpath(__APP__ . '/catalog.php');

// Если выход в контроллере не выполнен, значит путь неверный - вернуть 404
require_once realpath(__APP__ . '/404.php');