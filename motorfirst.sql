-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 27 2018 г., 23:30
-- Версия сервера: 10.1.26-MariaDB-0+deb9u1
-- Версия PHP: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `motorfirst`
--

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE `catalog` (
  `id` smallint(6) NOT NULL,
  `parent_id` smallint(6) NOT NULL,
  `name` varchar(256) NOT NULL,
  `path` varchar(32) NOT NULL,
  `descr` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`id`, `parent_id`, `name`, `path`, `descr`) VALUES
(1, 0, 'Каталог', 'catalog', 'Выберите интересующую вас категорию или конкретный товар'),
(2, 1, 'Телевизоры', 'tv', 'Каталог телевизоров'),
(3, 1, 'DVD-проигрыватели', 'dvd', 'Каталог DVD-проигрывателей'),
(4, 2, 'Samsung', 'samsung', 'Каталог товаров от торговой марки Samsung'),
(5, 4, 'Телевизор Series 6', 's6', 'Стильные и недорогие телевизоры Samsung серии S6'),
(6, 4, 'Телевизор Series 7', 's7', 'Функциональные телевизоры Samsung серии S7 для ценителей кино'),
(7, 3, 'LG', 'lg', 'Каталог товаров от торговой марки LG'),
(8, 7, 'LG DVD', 'lgdvd', 'DVD-проигрыватели LG с функцией записи на диск');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`parent_id`,`path`) USING BTREE,
  ADD KEY `parent_id` (`parent_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
