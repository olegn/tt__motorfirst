<?php
/**
 * Конвертор массива в XML дерево.
 * @param (array)  $data
 * @param (object) $xml_data
 * @param (string) $key_num
 * @return void
 */
function array_to_xml($data, &$xml_data, $key_num = 'item') {
  foreach($data as $key => $value) {
    if(strpos($key, '@') === 0){
      $key = substr($key, 1);
      $xml_data->addAttribute($key, $value);
    } else {
      $key = explode(':', $key);

      if (isset($key[1]) && !empty($key[1])) {
        $key_num = $key[1];
      } /* else {
        $key_num = 'item';
      } */

      if(is_numeric($key[0])){
        $key = $key_num;
      } else {
        $key = $key[0];
      }

      if(is_array($value)) {
        $subnode = $xml_data->addChild($key);
        array_to_xml($value, $subnode, $key_num);
      } else {
        $xml_data->addChild($key, $value);
      }
    }
  }
}

/**
 * Обработчик XSL шаблона.
 * @param (string) $xml_doc
 * @param (string) $xsl_doc
 * @return string
 */
function xslt($xml_doc, $xsl_doc) {
  $xml = new DOMDocument;
  if ($xml_doc_file = realpath($xml_doc)) {
    $xml->load($xml_doc_file);
  } else {
    $xml->loadXML($xml_doc);
  }

  $xsl = new DOMDocument;
  $xsl->load($xsl_doc);

  $proc = new XSLTProcessor;

  $proc->importStyleSheet($xsl);

  return $proc->transformToXML($xml);
}

/**
 * Конструктор многомерного массива с позициями каталога.
 * @param (array) $cats
 * @param (int)   $parent_id
 * @return array
 */
function tree_catalog($cats, $parent_id)
{
  if (is_array($cats) && isset($cats[$parent_id])) {
    $tree = array();
    foreach ($cats[$parent_id] as $k => $cat) {
      $tree[] = array('@id' => $k, 'info' => $cat['info'], 'childs' => tree_catalog($cats, $k));
    }
  } else {
    return false;
  }

  return $tree;
}

/**
 * Проверка uri на соответствие дереву.
 * @param (array) $tree
 * @param (array)   $uri
 * @return string
 */
function tree_path($tree, $uri) {
  $path = '';
  $id;
  $info;
  $check = false;

  for ($i = 1, $c = count($uri); $i < $c; $i++) {
    foreach ($tree as $v) {
      if ($v['info']['path'] == $uri[$i]) {
        $path .= '/' . $v['info']['path'];
        $check = true;
        if (is_array($v['childs'])) {
          $tree = $v['childs'];
        }
        $id = $v['@id'];
        $info = $v['info'];
        break;
      } else {
        $check = false;
      }
    }
    
    if ($check === false) {
      break;
    }
  }

  if ($check === true) {
    $path = array('path' => $path, 'item_id' => $id, 'item_info' => $info);
    return $path;
  } else {
    return false;
  }
}