<?php
// Данные для подключения к базе данных
$db_srvr = "localhost";
$db_name = "motorfirst";
$db_user = "motorfirst";
$db_pass = "";
$db_port = null;

// Создание подключения к базе данных
$mysqli = new mysqli($db_srvr, $db_user, $db_pass, $db_name, $db_port);

// Проверка ошибок подключения к базе данных
if ($mysqli->connect_errno) {
  echo "Извините, возникла проблема на сайте";

  echo "Ошибка: Не удалсь создать соединение с базой MySQL: \n";
  echo "Номер_ошибки: " . $mysqli->connect_errno . "\n";
  echo "Ошибка: " . $mysqli->connect_error . "\n";

  exit;
}

if (!$mysqli->set_charset("utf8")) {
  printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
  exit();
}
