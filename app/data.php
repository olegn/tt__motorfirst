<?php // model
// Подготовить запрос
$sql = "
  SELECT
    `id`, `parent_id`, `name`, `path`, `descr`
  FROM
    `catalog`
  ORDER BY
    `parent_id` ASC
";

// Выполнить запрос
$result = $mysqli->query($sql) or die("Запрос не удался: " . $mysqli->errno . " - " . $mysqli->error);

// Массив с позициями каталога
$catalog = array();

// Обработка данных из БД (формирование массива)
while ($item = $result->fetch_assoc()) {
  if (!isset($catalog[$item['parent_id']])) {
    $catalog[$item['parent_id']] = array();
  }

  $catalog[$item['parent_id']][$item['id']] = array('info' => array('path' => $item['path'], 'name' => $item['name'], 'descr' => $item['descr']), 'childs' => array());
}