<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" version="5" doctype-system="about:legacy-compat" encoding="UTF-8" include-content-type="no" indent="no" />

  <xsl:template match="/">

    <html class="html" lang="ru-RU">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- <link rel="icon" href="/favicon.png" sizes="16x16" /> -->

        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Material+Icons|Fira+Sans:400,500,400i&amp;subset=cyrillic" />
        <link rel="stylesheet" href="/common.css" />

        <!-- <script src="//yastatic.net/jquery/3.1.1/jquery.min.js"></script> -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->
        <!-- <script src="/scripts/libs/jquery-3.2.1.min.js"></script> -->
        <!-- <script src="/scripts/common.js"></script> -->
        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->

        <title>Построение дерева</title>
      </head>
      <body class="body">
        <div class="section main">
          <div class="section__content section__content_fixed_864">
            <div class="grid">

              <div class="grid__col grid__col_4">
                <div class="grid__content grid__content_gutter_24">
                
                  <ul class="tree">
                    <xsl:apply-templates select="/app/tree/item" mode="menu" />
                  </ul>

                </div>
              </div>

              <div class="grid__col grid__col_8">
                <div class="grid__content grid__content_gutter_24">
                
                  <div class="content">
                    <h2 class="content__head">
                      <xsl:value-of select="/app/path/item_info/name" />
                    </h2>
                    <p class="content__description">
                      <xsl:value-of select="/app/path/item_info/descr" />
                    </p>
                    <xsl:if test="count(//item[@id=/app/path/item_id]/childs/item) > 0">
                      <div class="content__cats">
                        <div class="grid">
                          <xsl:apply-templates select="//item[@id=/app/path/item_id]/childs/item" mode="content" />
                        </div>
                      </div>
                    </xsl:if>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </body>
    </html>

  </xsl:template>

  <xsl:template match="item" mode="menu">
    <xsl:param name="path" select="''" />
    <xsl:variable name="_path" select="concat($path, '/', info/path)" />

    <li class="tree__item">
      <a class="tree__link tree__link_state_hover" href="{$_path}">
        <xsl:if test="info/path = /app/path/item_info/path">
          <xsl:attribute name="class">
            <xsl:text>tree__link tree__link_state_active</xsl:text>
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="info/name" />
      </a>
      <xsl:if test="count(childs/item) > 0">
        <ul class="tree">
          <xsl:apply-templates select="childs/item" mode="menu">
            <xsl:with-param name="path" select="$_path" />
          </xsl:apply-templates>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template match="item" mode="content">
    <div class="grid__col grid__col_6">
      <div class="grid__content">
        <div class="content__catsItem">
          <a class="content__catsLink" href="{concat(/app/path/path, '/', info/path)}">
            <xsl:value-of select="info/name" />
          </a>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>