<?php // controller
require_once realpath(__APP__ . '/data.php');

$catalog = tree_catalog($catalog, 0);

$path = tree_path($catalog, $URI);

// print_r($path);die;

// print_r($catalog);die;
// print_r(tree_catalog($catalog, 0));die;
// var_dump(array_search('tv', $catalog));

if ($path !== false) {
  $dom = new SimpleXMLElement('<?xml version="1.0"?><app></app>');

  $catalog = array('tree' => $catalog);
  array_to_xml($catalog, $dom);

  $path = array('path' => $path);
  array_to_xml($path, $dom);

  // echo $dom->saveXML();die;

  echo xslt($dom->saveXML(), __APP__ . '/html.xsl');

  exit(0);
}